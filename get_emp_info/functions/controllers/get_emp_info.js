const COLLECTION = "emp_info_active";

const getEmpInfoActive = (req, res, firestore) => {
  const responseData = {
    result: true,
    status: 200,
    message: "",
    datas: []
  };

  if (!req.body.emp_id) {
    res
      .status(422)
      .send({
        result: false,
        message: 'Invalid Parameters'
      })
  }

  const emp_id = req.body.emp_id

  firestore.collection(COLLECTION)
    .where('emp_id', '==', emp_id)
    .get()
    .then(snapshot => {

      if (snapshot.empty) {

        responseData.result = true
        responseData.status = 404
        responseData.message = "Emp_id not found"

        res.status(404).send(responseData);
        console.log('No matching documents.');
        return;
      }

      snapshot.forEach(doc => {
        // console.log(doc.id, '=>', doc.data());
        const result = doc.data();

        const result2 = {
          emp_id: result.emp_id,
          name_th: result.emp_name,
          email: result.email,
          position: {
            id: result.posi_id,
            description: result.posi_desc
          },
          dept: {
            id: result.dept_id,
            description: result.dept_desc
          },
          duty: {
            id: result.duty,
            description: result.duty_desc
          }
        }

        responseData.datas = result2
        responseData.message = 'success'

        res.status(200).send(responseData)
      });
    })
    .catch(err => {
      console.log('Error getting documents', err);
    });


}


module.exports.handler = getEmpInfoActive;