'use strict';
// const express = require('express');
const cors = require('cors')({
  origin: true
});
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const request = require("request-promise");
// const app = express();

admin.initializeApp(functions.config().firebase);

var db = admin.firestore();



const getEmpInfoActive = require('./controllers/get_emp_info');


exports.getEmpInfoActive = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    getEmpInfoActive.handler(req, res, db);
  });
});