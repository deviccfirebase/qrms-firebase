var moment = require("moment-timezone");

const COLLECTION = "fair_master_device";

const fairMasterDevice = (req, res, firestore) => {

  const responseData = {
    result: true,
    status: 200,
    message: "",
    datas: []
  };

  const {

    device_number

  } = req.body


  console.log("Body ==== > > ", req.body);

  if (!req.body.device_number) {
    res.status(422).send({
      result: false,
      message: 'Invalid Parameters'
    })
  }

  firestore.collection(COLLECTION).where("device_number", "==", device_number)
    .get()
    .then(snapshot => {
      if (snapshot.empty) {

        responseData.result = true
        responseData.status = 404
        responseData.message = "Device not found"

        res.status(404).send(responseData);
        console.log("No matching documents.");

        return;
      }


      snapshot.forEach(doc => {
        // console.log("RESULT", doc.id, "=>", doc.data());

        const result = doc.data();

        const result2 = {

          fair_no: result.fair_no.toString(),
          fair_name: result.fair_name,
          device_number: result.device_number,
          begin_date: result.begin_date,
          end_date: result.end_date,
          product_list: result.product_list,
          ship_list: result.ship_list

        }

        responseData.datas = result2
        responseData.message = 'success'

      });



      res.status(200).send(responseData)
    })
    .catch(err => {
      console.log("Error getting documents", err);

      responseData.result = false
      responseData.status = 500
      responseData.message = err

      res.status(500).send(responseData);
    });
};

module.exports.handler = fairMasterDevice;