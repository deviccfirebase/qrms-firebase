'use strict';
// const express = require('express');
const cors = require('cors')({
  origin: true
});
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const request = require("request-promise");
// const app = express();

admin.initializeApp(functions.config().firebase);

var db = admin.firestore();



const getFairMasterItemCategory = require('./controllers/fair_master_item_category');


exports.getFairMasterItemCategory = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    getFairMasterItemCategory.handler(req, res, db);
  });
});