var moment = require('moment-timezone');

const COLLECTION = "fair_master_item_category";

const fairMasterItemCategory = (req, res, firestore) => {
  const responseData = {
    result: true,
    status: 200,
    message: "",
    datas: []
  };

  const {

    fair_no

  } = req.body

  // const fair_no = '1536'

  // console.log("fair_no", fair_no);

  try {
    return firestore.collection(COLLECTION).doc(fair_no)
      .get()
      .then(doc => {
        if (!doc.exists) {
          console.log('No such document!');

          responseData.result = true
          responseData.status = 404
          responseData.message = "Get Category not found"

          res.status(404).send(responseData);
          console.log("No matching documents.");

          return;
        } else {
          const result = doc.data();
          // console.log('Category data:', result);

          const level = []

          result.level1.forEach((element1, index1) => {
            level.push({
              category_name: element1.category_name,
              level: []
            })
            element1.level2.forEach((element2, index2) => {
              level[index1].level.push({
                category_name: element2.category_name,
                level: []
              })
              element2.level3.forEach((element3, index3) => {
                level[index1].level[index2].level.push({
                  category_name: element3.category_name,
                  level: []
                })
                element3.level4.forEach((element4, index4) => {
                  level[index1].level[index2].level[index3].level.push({
                    category_name: element4.category_name,
                    level: []
                  })
                });
              });
            });
          });



          const result2 = {

            fair_no: result.fair_no.toString(),
            level: level

          }

          responseData.datas = result2
          responseData.message = 'success'

          res.status(200).send(responseData)

        }
      })
      .catch(err => {
        console.log('Error getting Category', err);

        responseData.result = false
        responseData.status = 500
        responseData.message = "Erro Unknow"

        return res.status(500).send(responseData);
      });
  } catch (error) {
    console.log('Error getting Category', error);

    responseData.result = false
    responseData.status = 500
    responseData.message = "Erro Unknow"

    return res.status(500).send(responseData);
  }


}


module.exports.handler = fairMasterItemCategory;