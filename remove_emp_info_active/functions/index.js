'use strict';
// const express = require('express');
const cors = require('cors')({
  origin: true
});
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const request = require("request-promise");
// const app = express();

admin.initializeApp(functions.config().firebase);

var db = admin.firestore();



const removeEmpInfoActive = require('./controllers/remove_emp_info_active');


exports.removeEmpInfoActive = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    removeEmpInfoActive.handler(req, res, db);
  });
});


exports.removeEmpInfoActiveScheduler = functions
  .pubsub.schedule("30 5 * * *")
  .timeZone('Asia/Bangkok')
  .onRun((context) => {
    console.info("This will be run 05:30 AM ");
    return request({
      method: "POST",
      uri: `https://us-central1-qrms-icc.cloudfunctions.net/removeEmpInfoActive`,
    }).then(() => {
      return console.info("removeEmpInfoActiveScheduler Done");
    }).catch(error => {
      return Promise.reject(error);
    });
  });  