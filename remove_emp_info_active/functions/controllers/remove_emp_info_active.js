var moment = require("moment-timezone");

const COLLECTION = "emp_info_active";

const removeEmpInfoActive = (req, res, firestore) => {

  const responseData = {
    result: true,
    status: 200,
    message: "",
    datas: []
  };


  const date_now = moment.tz("Asia/Bangkok").subtract(1, "days").format('YYYYMMDD')

  console.log(date_now);


  firestore.collection(COLLECTION).where("date_resign", "<=", date_now)
    .get()
    .then(snapshot => {
      if (snapshot.empty) {

        responseData.result = true
        responseData.status = 404
        responseData.message = "Data not found"

        res.status(404).send(responseData);
        console.log("No matching documents.");

        return;
      }

      let datas = []

      let count = 0;

      snapshot.forEach(doc => {
        // console.log(doc.id, "=>", doc.data());

        count++

        const result = doc.data();

        const result2 = {

          emp_id: result.emp_id,
        }
        datas.push(result2)

        const delEmp = firestore
          .collection(COLLECTION)
          .doc(result.emp_id)
          .delete();

        console.log('Deleted Emp InfoActive', + result.emp_id);

      });

      responseData.datas = datas
      responseData.message = 'delete success count= ' + count

      if (req) {

        res.status(200).send(responseData)

      }


    })
    .catch(err => {
      console.log("Error getting documents", err);

      responseData.result = false
      responseData.status = 500
      responseData.message = err

      if (req) {

        res.status(500).send(responseData);

      }


    });
};

module.exports.handler = removeEmpInfoActive;