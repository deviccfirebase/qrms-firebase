var moment = require('moment-timezone');

const COLLECTION = "fair_announce";

const fairAnnounce = (req, res, firestore) => {
  const responseData = {
    result: true,
    status: 200,
    message: "",
    datas: []
  };

  firestore.collection(COLLECTION).doc('slide')
    .get()
    .then(doc => {
      if (!doc.exists) {
        console.log('No such document!');

        responseData.result = true
        responseData.status = 404
        responseData.message = "Get announce not found"

        res.status(404).send(responseData);
        console.log("No matching documents.");

        return;
      } else {
        // console.log('Document data:', doc.data());
        const result = doc.data().datas;

        let resMap = result.map((item, index) => {

          return {
            rowno: index + 1,
            announce_id: item.announce_id,
            picture: item.picture,
            url: item.url

          };
        });

        responseData.datas = resMap
        responseData.message = 'success'

        res.status(200).send(responseData)

      }
    })
    .catch(err => {
      console.log('Error getting document', err);
    });
}


module.exports.handler = fairAnnounce;