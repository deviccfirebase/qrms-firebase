var moment = require("moment-timezone");

const COLLECTION = "fair_master_item_bill";

const fairMasterItemBill = (req, res, firestore) => {

  const responseData = {
    result: true,
    status: 200,
    message: "",
    datas: []
  };

  const {

    fair_no,
    ship_no,
    product_code,
    corner_id

  } = req.body


  console.log("Body ==== > > ", req.body);

  if (!req.body.fair_no || !req.body.ship_no || !req.body.product_code || !req.body.corner_id) {
    res.status(422).send({
      result: false,
      message: 'Invalid Parameters'
    })
  }

  var counterid = product_code + corner_id

  console.log(counterid);

  firestore.collection(COLLECTION).doc(fair_no).collection(ship_no)
    .where("counterid", "==", counterid)
    .get()
    .then(snapshot => {
      if (snapshot.empty) {

        responseData.result = true
        responseData.status = 404
        responseData.message = "Item bill not found"

        res.status(404).send(responseData);
        console.log("No matching documents.");

        return;
      }

      let datas = []

      snapshot.forEach(doc => {
        // console.log(doc.id, "=>", doc.data());

        const result = doc.data();

        let resMap = result.items.map((item, index) => {

          return {
            rowno: index + 1,
            barcode: item.barcode,
            color_code: item.color_code,
            picture: `https://xxx.icc.co.th/${result.product_code}/${result.items[index].item_id}`

          };
        });

        const result2 = {

          fair_no: result.fair_no.toString(),
          ship_no: result.ship_no,
          product_code: result.product_code,
          corner_id: result.corner_id,
          counterid: result.counterid,
          items: resMap
        }

        // datas.push(result2)
        responseData.datas = result2
        responseData.message = 'success'

        res.status(200).send(responseData)
      });

    })
    .catch(err => {
      console.log("Error getting documents", err);

      responseData.result = false
      responseData.status = 500
      responseData.message = err

      res.status(500).send(responseData);
    });
};

module.exports.handler = fairMasterItemBill;