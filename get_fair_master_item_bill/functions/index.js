'use strict';
// const express = require('express');
const cors = require('cors')({
  origin: true
});
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const request = require("request-promise");
// const app = express();

admin.initializeApp(functions.config().firebase);

var db = admin.firestore();

const getFairMasterItemBill = require('./controllers/fair_master_item_bill');

exports.getFairMasterItemBill = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    getFairMasterItemBill.handler(req, res, db);
  });
});