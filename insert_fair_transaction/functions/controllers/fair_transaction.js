var moment = require("moment-timezone");

const COLLECTION = "fair_transaction";

const insfairMasterTransaction = (req, res, firestore) => {

  const responseData = {
    result: true,
    status: 200,
    message: "",
    datas: []
  };

  const transaction = req.body


  console.log("Body ==== > > ", req.body);

  if (!transaction) {
    res.status(422).send({
      result: false,
      message: 'Invalid Parameters'
    })
  }

  transaction.flag = 'N'

  firestore.collection(COLLECTION).doc(transaction.fair_no.toString()).collection(transaction.ship_no).doc(transaction.store_id + '_' + transaction.branch_id + '_' + transaction.counter_id + '_' + transaction.ship_no + '_' + transaction.trans_type + '_' + transaction.doc_no).set(transaction)
    .then(function () {

      console.log('Send Fair Transaction =>', transaction.doc_no)

      responseData.datas = transaction
      responseData.message = 'success'

      res.status(200).send(responseData)



    })
    .catch(err => {
      console.log("Error getting documents", err);

      responseData.result = false
      responseData.status = 500
      responseData.message = err

      res.status(500).send(responseData);
    });
};

module.exports.handler = insfairMasterTransaction;