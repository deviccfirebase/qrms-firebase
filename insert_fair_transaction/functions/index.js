'use strict';
// const express = require('express');
const cors = require('cors')({
  origin: true
});
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const request = require("request-promise");
// const app = express();

admin.initializeApp(functions.config().firebase);

var db = admin.firestore();



const insertFairMasterTransaction = require('./controllers/fair_transaction');


exports.insertFairMasterTransaction = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    insertFairMasterTransaction.handler(req, res, db);
  });
});