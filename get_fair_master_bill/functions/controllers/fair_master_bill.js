var moment = require("moment-timezone");

const COLLECTION = "fair_master_bill";

const fairMasterBill = (req, res, firestore) => {

  const responseData = {
    result: true,
    status: 200,
    message: "",
    datas: []
  };

  const {

    fair_no,
    ship_no,
    product_code,
    corner_id

  } = req.body


  console.log("Body ==== > > ", req.body);

  if (!req.body.fair_no || !req.body.ship_no || !req.body.product_code || !req.body.corner_id) {
    res.status(422).send({
      result: false,
      message: 'Invalid Parameters'
    })
  }


  firestore.collection(COLLECTION).doc(fair_no).collection(ship_no)
    .where("product_code", "==", product_code)
    .where("corner_id", "==", corner_id)
    .get()
    .then(snapshot => {
      if (snapshot.empty) {

        responseData.result = true
        responseData.status = 404
        responseData.message = "Bill not found"

        res.status(404).send(responseData);
        console.log("No matching documents.");

        return;
      }

      let datas = []

      snapshot.forEach(doc => {
        // console.log(doc.id, "=>", doc.data());

        const result = doc.data();

        const result2 = {

          fair_no: result.fair_no.toString(),
          bill_no: result.bill_no,
          product_code: result.product_code,
          ship_no: result.ship_no,
          wh_system: result.wh_system,
          corner_id: result.corner_id,
          site_id: result.site_id,
          bill_n8: result.bill_n8,
          net_amount: result.net_amount,
          bill_date: result.bill_date,
          total_qty: result.total_qty,
          items: result.items
        }
        datas.push(result2)

      });
      responseData.datas = datas
      responseData.message = 'success'

      res.status(200).send(responseData)
    })
    .catch(err => {
      console.log("Error getting documents", err);

      responseData.result = false
      responseData.status = 500
      responseData.message = err

      res.status(500).send(responseData);
    });
};

module.exports.handler = fairMasterBill;